
1. [ ] Check `PLATFORM_SECURITY_PATCH` value is equal to the last `Security patch level` available at [source.android.com](https://source.android.com/docs/security/bulletin/asb-overview#bulletins)
   - [ ] Android 10 (~q): [version_defaults.mk](https://gitlab.e.foundation/e/os/android_build/-/blob/v1-q/core/version_defaults.mk?ref_type=heads#L253)
   - [ ] Android 11 (~r): [version_defaults.mk](https://gitlab.e.foundation/e/os/android_build/-/blob/v1-r/core/version_defaults.mk?ref_type=heads#L243)
   - [ ] Android 12 (~s): [version_defaults.mk](https://gitlab.e.foundation/e/os/android_build/-/blob/v1-s/core/version_defaults.mk?ref_type=heads#L243)
   - [ ] Android 13 (~t): [version_defaults.mk](https://gitlab.e.foundation/e/os/android_build/-/blob/v1-t/core/version_defaults.mk?ref_type=heads#L106)
2. [ ] Check with dev that every prebuilt apk are updated (+AdvancedPrivacy)
3. [ ] Update OpenKeyChain and MagicEarth
   - [ ] OpenKeychain
   - [ ] MagicEarth
4. [ ] Update `PRODUCT_VERSION_MAJOR`, `PRODUCT_VERSION_MINOR` or `PRODUCT_VERSION_MINOR`in [android_vendor_lineage](https://gitlab.e.foundation/e/os/android_vendor_lineage)
   - [ ] Android 10 (~q): [common.mk](https://gitlab.e.foundation/-/ide/project/e/os/android_vendor_lineage/edit/v1-q/-/config/common.mk)
   - [ ] Android 11 (~r): [common.mk](https://gitlab.e.foundation/-/ide/project/e/os/android_vendor_lineage/edit/v1-r/-/config/common.mk)
   - [ ] Android 12 (~s): [version.mk](https://gitlab.e.foundation/-/ide/project/e/os/android_vendor_lineage/edit/v1-s/-/config/version.mk)
   - [ ] Android 13 (~t): [version.mk](https://gitlab.e.foundation/-/ide/project/e/os/android_vendor_lineage/edit/v1-t/-/config/version.mk)
5. [ ] Merge the [changelog merge request](https://gitlab.e.foundation/e/os/changelog/-/merge_requests) **if it has been approved by all reviewers**
6. [ ] Update manifests using [schedule jobs](https://gitlab.e.foundation/e/os/releases/-/pipeline_schedules)
   - [ ] Android 10 (~q): `Update Q manifest` ▶️
   - [ ] Android 11 (~r): `Update R manifest` ▶️
   - [ ] Android 12 (~s): `Update S manifest` ▶️
   - [ ] Android 13 (~t): `Update T manifest` ▶️
7. [ ] Create [release tags](https://gitlab.e.foundation/e/os/releases/-/tags/new)
   - [ ] Android 10 (~q): tag name is `vX.Y.Z-beta-q`, create from `v1-q`
   - [ ] Android 11 (~r): tag name is `vX.Y.Z-beta-r`, create from `v1-r`
   - [ ] Android 12 (~s): tag name is `vX.Y.Z-beta-s`, create from `v1-s`
   - [ ] Android 13 (~t): tag name is `vX.Y.Z-beta-t`, create from `v1-t`
8. [ ] Start jobs from [tag pipelines](https://gitlab.e.foundation/e/os/releases/-/pipelines?scope=tags&page=1)
   - [ ] Android 10 (~q): `trigger-build-q-test` and `trigger-build-q-stable`
   - [ ] Android 11 (~r): `trigger-build-r-test` and `trigger-build-r-stable`
   - [ ] Android 12 (~s): `trigger-build-s-test` and `trigger-build-s-stable`
   - [ ] Android 13 (~t): `trigger-build-t-test` and `trigger-build-t-stable`
