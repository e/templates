- /e/OS version:
- Device model(s):
- Impacted Application: ~
- Affected application/URL:
- Browser/client and version:

## The problem

**Describe precisely the problem you're facing and add screenshots to illustrate**



**Steps to reproduce**


## Technical details

**Paste any relevant logs (`adb logcat`) in the codeblock below if you have any**

```

```

/label ~"type::Bug"
<!--Add the labels corresponding to your issue by adding a tilde and typing the name of the label you think apply to your issue in the line above. You need to type a tilde before the name of each label you want to apply to the issue. -->

