- /e/ version:
- Device model(s):

## Summary

<!-- Summarize the improvement briefly and precisely -->

## Description

**What is the current behavior?**

<!-- What actually happens -->

**What is the improved behavior?**

<!-- What you should see instead -->

**What does it bring?**

<!-- Why this improvement is needed -->

## Mock-ups

<!-- Provide a mock-up of how it would look with the improvement -->

## Examples

<!-- Give the example of what users will be able to accomplish with the improvement -->

## Validation

<!-- List test case that will be run to validate that the issue is working as expected -->

/label ~"type::User story"

<!-- Add the labels corresponding to your issue by adding a tilde and typing the name of the label you think apply to your issue in the line above. You need to type a tilde before the name of each label you want to apply to the issue. -->
