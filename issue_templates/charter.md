The use of “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” is per the IETF standard defined in RFC2119.

# Hardware Support

## Audio

* [ ] The device MUST support audio playback for media content.
* [ ] Phone MUST support in-call audio.
* [ ] Phone MUST support speaker audio.
* [ ] Devices SHOULD support any additional audio configuration inherent to their device (eg. echo cancellation, extra mics, etc).
* [ ] The device MUST support any other audio output/input supported by their stock OS (eg. headphone jack, USB-C, BT, microphone).
* [ ] The device with FM radio capabilities in their stock OS SHOULD support FM.

## RIL

* [ ] The device with RIL supported in their stock OS MUST support RIL for phone calls, data, SMS and MMS.
* [ ] The device with RIL supported in their stock OS MUST support emergency calling with a SIM inserted (112/911).
* [ ] The device with RIL supported in their stock OS SHOULD support emergency calling without a SIM inserted (112/911).
* [ ] Data-only device (defined as devices that have a RIL but do not support telephony stack due to hardware/firmware restrictions) are EXEMPTED from phone & emergency dialing requirements.
* [ ] Devices SHOULD support VoLTE & VoNR if supported by their stock OS
* [ ] Devices SHOULD support VoWIFI if supported by their stock OS
* [ ] Devices SHOULD support eSIM if supported by their stock OS
* [ ] Devices SHOULD disable world phone bool in their CarrierConfig

## Encryption

* [ ] The device that supported hardware-backed encryption on their stock OS MUST support hardware-backed encryption.
* [ ] The device that shipped stock as forceencrypt SHOULD default to forceencrypt enabled.
* [ ] The device MUST support software encryption.

## Wi-Fi

* [ ] The device with Wi-Fi supported in their stock OS MUST support Wi-Fi.
* [ ] The device with Wi-Fi MUST report same MAC address as on stock OS.
* [ ] The device with Wi-Fi hotspot capabilities MUST support Wi-Fi tethering.

## USB

* [ ] The device with a USB port MUST support file access via MTP.
* [ ] The device with USB tethering supported on their stock OS MUST support USB tethering.
* [ ] The device with a USB port & Data SHOULD support USB tethering.
* [ ] The device with Ethernet over USB supported on their stock OS MUST support Ethernet over USB

## GPS

* [ ] The device with GPS supported in their stock OS MUST support GPS.

## Bluetooth

* [ ] The device with Bluetooth supported in their stock OS MUST support Bluetooth.
* [ ] The device with Bluetooth MUST report same MAC address as on stock OS.
* [ ] The device with Bluetooth SHOULD support Bluetooth tethering.
* [ ] The device with support for Qualcomm® aptX™, aptX™ HD, or any future variant of aptX™, in stock (non-beta releases) OS SHOULD support those variant of aptX™.
* [ ] The device without support for Qualcomm® aptX™, aptX™ HD, or any future variant of aptX™ in stock (non-beta releases) OS MUST NOT support those variants of aptX™.

## Camera

* [ ] The device with Camera supported in their stock OS MUST support Camera, in both front facing and rear camera configurations.
* [ ] The device with Dual (or more) Rear Cameras SHOULD support all rear cameras.
* [ ] The device with Dual (or more) Front Facing Cameras SHOULD support all front cameras.
* [ ] All Camera HAL versions accessible with the device's Camera HAL MUST comply with the Camera and Video Recording requirements.

## Video Recording

* [ ] The device with Video Recording supported in their stock OS MUST support Video Recording, in both front facing and rear camera configurations.

## Codecs

* [ ] The device with hardware encoding/decoding support in their stock OS MUST support hardware encoding/decoding for all non-proprietary codecs supported by their stock OS.

## Display

* [ ] All devices with a built-in Display MUST support the Display at the same resolution as the stock OS.
* [ ] All devices with a built-in Display SHOULD support the Display at the same density as the stock OS.
* [ ] The device that do not include a built-in Display MUST support Display output via the hardware’s supported outputs (eg. Android TV - HDMI).
* [ ] The device that support additional non-USB display interfaces SHOULD support those display output methods.
* [ ] The device that support a USB-out display in their stock OS SHOULD support this display output (eg. MHL/Miracast/OTG).
* [ ] The device that support HDR10 playback in their stock OS SHOULD support HDR10 playback.

## NFC

* [ ] The device with NFC supported in their stock OS MUST support NFC.

## Fingerprint Sensor

* [ ] The device with a Fingerprint Sensor MUST support the Fingerprint Sensor if the stock OS supports it with Marshmallow or higher Android versions.
* [ ] The device with a Fingerprint Sensor SHOULD support the Fingerprint Sensor if the stock OS supports it for all other Android versions.
* [ ] The device with a Fingerprint Sensor MUST display properly the position of the sensor in the fingerprint setup wizard (available in the FTSW and in Settings > Security)

## IR

* [ ] The device with an IR blaster SHOULD support IR blaster.

## Accelerometer

* [ ] The device with an accelerometer MUST support the accelerometer.

## Gyroscope

* [ ] The device with a gyroscope MUST support the gyroscope.

## Proximity

* [ ] The device with a proximity sensor MUST support the proximity sensor.

## Light

* [ ] The device with a light sensor MUST support the light sensor.

## SDCard

* [ ] The device with a SDCard slot MUST support the SDCard.

## Other Sensors

* [ ] All other sensors supported by a device’s stock OS SHOULD be supported.

## Accessories

* [ ] The device with proprietary accessories SHOULD support those accessories (eg. O-Click, Essential 360 Camera).

## Charging

* [ ] Device SHOULD support any charging standard supported in the stock OS (Eg: Dash Charging, Quick Charge, Pump Express)
* [ ] Device MUST support offline charging.

## Widevine

* [ ] Device SHOULD support the highest supported Widevine security level as the stock OS

# Software support

## Device tree structure

* [ ] Device trees MUST contain a Lineage-specific makefile with device declaration of lineage_[devicename].
* [ ] Device trees MUST support a lineage.dependencies file for `breakfast` command & roomservice to be functional.
  * This file MUST NOT include any dependencies outside of the "LineageOS" organization.

## Build type

* [ ] The device MUST be configured as userdebug releases.

## Kernel

* [ ] The device MUST NOT ship a prebuilt kernel.
* [ ] The device MUST NOT implement software based touchscreen wake features such as double tap to wake, swipe to wake or gestures if there is no hardware-backed support for them in the touchscreen firmware.
* [ ] The device MUST NOT implement forced fast charge over USB methods that violate the USB specifications.
* [ ] The device MUST NOT implement any form of clock manipulation (underclocking, overclocking, etc.) for any processor (CPU, GPU).
* [ ] The device MUST NOT implement any form of hardware voltage manipulation (undervolting, custom voltage tables, etc.).
* [ ] The device MUST NOT implement any form of hardware register manipulation (sound control, etc.).
* [ ] The device MUST NOT implement any form of custom KSM driver (UKSM, etc.).
* [ ] The device MUST NOT ship governors other than the ones specified in the following list:
  * conservative
  * interactive
  * ondemand
  * performance
  * powersave
  * sched
  * schedutil
  * userspace

* [ ] The device MUST NOT ship I/O schedulers other than the ones specified in the following list:
  * bfq
  * cfq
  * deadline
  * noop
  * row

* [ ] The device MUST only ship hotplugging drivers provided by the OEM or SoC vendor.

## SELinux status

* [ ] The device MUST be configured for SELinux Enforcing.

## Verity

* [ ] The device MUST disable verity on the system image for userdebug builds.
* [ ] The device with AVB enabled and with a lockable bootloader SHOULD support verity on the vendor image.

## Updater

* [ ] The device with a shipping build of LineageOS MUST support upgrades via the native LineageOS Updater application & the recovery documented on the Wiki for that device.

## FRP

* [ ] The device with stock support of Factory Reset Protection (FRP) SHOULD support FRP when Google Applications are installed by the user.

## SafetyNet

* [ ] The device MUST pass SafetyNet Attestation
* [ ] The device MUST pass the checks made by Rootbeer app.

## Binder

* [ ] The device MUST use the 64-bit Binder API.

## Root (su)

* [ ] The device MUST NOT ship with su included.
* [ ] The device shipping LineageOS 16.0 or earlier MUST support su installation via LineageOS provided ‘Extras’ download.

## Non-PIE Blobs

* [ ] Devices MUST NOT use non-PIE (position-independent executable) binaries.

## Proprietary files extraction

* [ ] Devices MUST have a working proprietary files extraction script in their device tree (or device tree dependencies) that reproduces an exact copy of the binaries required to build LineageOS from an existing LineageOS installation.
* [ ] Devices SHOULD use the global extraction script (located in tools/extract-utils).
* [ ] If a device maintainer elects to not use the common extraction script, the maintainer MUST ensure that the Wiki page for their device has valid instructions for operating the custom extraction script.
* [ ] Devices MUST use proprietary files extracted from, in order of preference, the same device's publicly-released image, another device's publicly-released image, or some other source with appropriately transferrable use/release/dissemination rights. In the event of the last option, artifacts documenting suitable transferability of rights MUST be provided to LineageOS project leadership.
* [ ] All proprietary files lists MUST contain a short comment noting the source of any un-pinned (default) proprietary files.
* [ ] Any proprietary files not sourced from the noted default MUST be pinned in the respective proprietary files list and have a short comment noting the source of those proprietary files.
* [ ] Devices MUST NOT include blobs belonging to Megvii Technology Ltd. or SenseTime Group Ltd.
* [ ] Devices MUST NOT include Google Hotword (Ok Google) blobs.

## Firmware

* [ ] The device MUST assert on known to be working firmware versions if some firmware versions are known to be non-working.
* [ ] A/B devices are exempted from the above rule, and instead must do one of the following:
  * [ ] If the device is capable of shipping firmware it MUST do so.
  * [ ] If the device is not capable of shipping firmware (eg. a device with multiple variants supported in one build) the device MUST ensure both slots are on a known good firmware (eg. using the copy-partitions before_lineage_install template on the Wiki).

## exFAT Support

__LineageOS operates under the assumption that OEM device licensing for exFAT is attached to the device, not software. LineageOS will comply with all requests for removal of exFAT support from OEMs, Microsoft or their representatives upon contact to legal@lineageos.org.__

* [ ] The device with exFAT support on stock MAY support exFAT with (and only with) a kernel based implementation.
* [ ] The device without exFAT support on stock MUST NOT support exFAT.

## Additional Features

* [ ] The device SHOULD support in-kernel (MDSS, MDNIE or similar) LiveDisplay colour adjustment.

## Vendor Images

* [ ] All non-A/B devices relying on an OEM provided vendor partition must assert vendor image versions at flash-time.
* [ ] All A/B devices with a vendor partition MUST either:
  * [ ] Build a vendor image.
  * [ ] Detail on the Wiki: The required firmware version, the process of installing it, and a process that ensures all slots are on the same firmware (eg. the Wiki's copy-partitions template).
* [ ] All maintainers SHOULD NOT require a modified prebuilt vendor image either in their build tree, or on the Wiki.
  * [ ] If required, the maintainer MUST mention modifications in the readme of the device specific folder
* [ ] All Treble enabled devices SHOULD verify basic hardware functionality with an /e/OS GSI.

## Adaptable storage

* [ ] All devices with internal storage >= 64GB MUST disable adaptable storage

# Quality of life
## Commit Authorship

* [ ] All non-original commits MUST have proper authorship attribution from the source it was taken from or adapted from.

## Workflow

* [ ] Force pushing branches SHOULD be avoided.
* [ ] In the event of a force pushed branch, backup branches of the pre-forced HEAD MUST be made.

## Licensing

* [ ] All Kernel contributions MUST be GPLv2.
* [ ] All Android contributions SHOULD be Apache 2.0 licensed.
* [ ] Any contribution to an existing Apache 2.0 project MUST fall under Apache Compliance Category A.
* [ ] Any contribution to an existing Apache 2.0 project MUST NOT be in Apache Compliance Category X.

## Stability

* [ ] Issues like the "screen of death" MUST NOT affect the device.
* [ ] The device MUST NOT have abnormal battery drain

## Recovery

* [ ] Maintainers having recovery as the primary installation mechanism, MUST ensure the precense of a valid recovery image and verify instructions on the wiki.
* [ ] Devices that do not have traditional Recovery images MUST support & document another means of installation for /e/ OS zip files.
* [ ] Maintainers wishing to ship /e/ OS ~q or later MUST ship /e/ OS recovery as the default solution for their device.
* [ ] Maintainers wishing to ship /e/ OS ~q or later for their device MUST verify functionality of /e/ OS Recovery. This is including but not limited to:
  * [ ] Installation through ADB sideload
  * [ ] Format data/Factory reset
  * [ ] Enable ADB toggle (userdebug builds)
  * [ ] fastbootd (Only applicable to devices who launched with Android 10 or higher on the stock OS)
* [ ] Devices that do not have recovery as boot, MUST update recovery on successive updates of /e/ OS.

✅ works

❌ doesn't work

⚠ can't test

Striked: not relevant

empty box: not tested

*Mostly based on https://github.com/LineageOS/charter/blob/master/device-support-requirements.md*
