🔗 Links

- [Iteration]()
- [Board]()
- [Issue list]()

<!-- Title: [SquadName] Sprint 2024 - W1/W2 -->

/iteration *iteration:

## 🛠 Scope definition

Targeted weight: [+Weight+]

<!-- goals that have been planned -->

- **Goal 1**
- **Goal 2**
- **Goal 3**
- **Goal 4**
- **Goal 5**
- **Goal 6**
- **Goal 7**
- **Goal 8**
- **Goal 9**
- **Goal 10**

## 🏎 Kickoff

- [ ] Confirm all issues have a weight
- [ ] Set a milestone on each issue
- [ ] Report all unachieved issues and goals from the previous sprint, as first prio
- [ ] Adjust the sprint weights based on
    - the weight the squad committed to: [+Weight+]
    - the squad availability for the sprint (days off): [-Days-off-]

<!-- goals that are part of the sprint scope -->

- **Goal 1**
- **Goal 2**
- **Goal 3**
- **Goal 4**
- **Goal 5**
- **Goal 6**
- **Goal 7**
- **Goal 8**
- **Goal 9**
- **Goal 10**

## 🏁 Closure

<!-- weight that have been done during the sprint -->

Total weight done: [-Weight-]
- ~"stage::in validation": 
- ~"stage::awaiting deployment": 
- closed: 
- moved: 

<!-- status for each goal at the end of the sprint -->

<!--✅: goal achieved, which means all issues are at least in validation-->

<!--❌: goal not achieved-->

- **Goal 1**
- **Goal 2**
- **Goal 3**
- **Goal 4**
- **Goal 5**
- **Goal 6**
- **Goal 7**
- **Goal 8**
- **Goal 9**
- **Goal 10**

## 📺 Retrospective

- **Team member 1**: 
- **Team member 2**: 
- **Team member 3**: 
- **Team member 4**: 
- **Team member 5**: 

/label ~"type::Sprint"