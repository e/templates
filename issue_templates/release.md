# 📃 Tasks

* [ ] Release notes
* [ ] Validation of <!--beta milestone -->
* [ ] Run community builds about <!--beta milestone -->
* [ ] Share <!--beta milestone --> with testing community
* [ ] Validation of <!--rc milestone -->
* [ ] Validation of <!--update milestone -->

# 📆 Release Plan

- [ ] Publish release notes
- [ ] Run jobs under `check-after-build` for ~t, ~s.

## day n

- [ ] Update the App Lounge APK at https://doc.e.foundation/support-topics/app_lounge#download-app-lounge-apks
- [ ] Update the ~one link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/one/one-en
- [ ] Release Documentation
- [ ] Open download for new devices
- [ ] Open download for device upgrade
- [ ] Update ~axolotl [documentation](https://doc.e.foundation/devices/axolotl/install#downloads-for-the-axolotl) if security patch level has changed
- [ ] Update ~FP4 [documentation](https://doc.e.foundation/devices/FP4/install#downloads-for-the-fp4) if security patch level has changed
- [ ] Update ~FP5 [documentation](https://doc.e.foundation/devices/FP5/install#downloads-for-the-fp5) if security patch level has changed
- [ ] 25% availability of ~t community builds
- [ ] 25% availability of ~s community builds
- [ ] Share the release on `/e/OS - Murena team` Telegram channel

## day n + 4

- [ ] 100% availability of ~t community builds
- [ ] 100% availability of ~s community builds
- [ ] Release to flash stations for ~t, ~s community builds (if applicable)
- [ ] 25% availability of ~t official builds
- [ ] 25% availability of ~s official builds
- [ ] 25% availability of ~t partner builds
- [ ] 25% availability of ~s partner builds

## day n + 8

- [ ] 100% availability of ~t official builds
- [ ] 100% availability of ~s official builds
- [ ] Release to flash stations for ~t, ~s official builds (if applicable)
- [ ] 100% availability of ~t partner builds
- [ ] 100% availability of ~s partner builds
- [ ] Release to flash stations for ~t, ~s partner builds (if applicable)
- [ ] Run jobs under `check-after-release` for ~t, ~s.
- [ ] Update the ~one link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/one/one-en
- [ ] Update the ~two link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/two/two-release
- [ ] Update the ~zirconia link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/eos-zirconia
- [ ] Update the ~emerald link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/emerald/eos
- [ ] Update the ~FP4 link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/fp4/fp4-edl-qfil-en
- [ ] Update the ~FP5 link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/FP5/FP5-edl-qfil
- [ ] Inform ESPRI

/label ~"type::Task"
/milestone %
