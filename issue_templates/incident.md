## Summary

<!-- What has happened and which users or features are affected -->

## Diagnosis

**Components affected**

[one of status.ecloud.global]: <> (Mail, eCloud, mariadb, gitlab, OTA/Images)

**Priority**

[Please assign a severity to the issue after submitting]: <>
(
S1 - critical - mail or nextcloud service fully down
S2 - high - some feature not available, like rainloop webmail or onlyoffice
S3 - medium - some annoyance like slow loading times, error notifications on the phone, intermitent access…
S4 - low - minor problem not preventing the use of the feature, with or without workaround. Eg icons being gone or no image previews
)

**Has it happened before?** 

[.]: <> (Is it the first time, or there were other occurrences of the issue?)

## Solutions

**Workaround**

[.]: <> (any alternative way the user goal can be achieved, at least partially)

**Possible fixes**

[.]: <> (commands to execute or changes to deploy)
