## Summary

<!-- Summarize the feature briefly and precisely -->

## Description

<!--Give several use case scenarios formated with the following pattern:-->

<!-- REQN: As a user..., I want to..., In order to... -->

<!-- Examples: -->

<!-- REQ1: As a user of App Lounge, I want to be able to Ignore the session refresh dialog, in order to not fall in a infinite loop when the token dispenser cannot provide me a token-->

<!-- REQ2: As a user of App Lounge, I want to be able to install an app by just clicking on the "Install" button right from the search results, in order to install several apps in a row-->

## Reflection

**Mockups**

**Diagrams**

## Validation

<!--List test case that will be run to validate that the issue is working as expected -->

/label ~"type::User story"

<!--Add the labels corresponding to your issue by adding a tilde and typing the name of the label you think apply to your issue in the line above. You need to type a tilde before the name of each label you want to apply to the issue. -->
