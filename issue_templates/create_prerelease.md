
1. [ ] Check with dev that every prebuilt apk are updated (+AdvancedPrivacy)
2. [ ] Merge the [changelog merge request](https://gitlab.e.foundation/e/os/changelog/-/merge_requests) **if it has been approved by all reviewers**
3. [ ] Update manifest with all MR assigned to the current milestone (join https://gitlab.e.foundation/dashboard/merge_requests and use the tag filter)
   - [ ] Android 10 (~q): [default.xml](https://gitlab.e.foundation/e/os/releases/-/blob/v1-q/default.xml)
   - [ ] Android 11 (~r): [default.xml](https://gitlab.e.foundation/e/os/releases/-/blob/v1-r/default.xml)
   - [ ] Android 12 (~s): [default.xml](https://gitlab.e.foundation/e/os/releases/-/blob/v1-s/default.xml)
   - [ ] Android 13 (~t): [default.xml](https://gitlab.e.foundation/e/os/releases/-/blob/v1-t/default.xml)
4. [ ] Create [release tags](https://gitlab.e.foundation/e/os/releases/-/tags/new)
   - [ ] Android 10 (~q): tag name is `vX.Y.Z-(beta|rc(.I))-q`, create from `v1-q`
   - [ ] Android 11 (~r): tag name is `vX.Y.Z-(beta|rc(.I))-r`, create from `v1-r`
   - [ ] Android 12 (~s): tag name is `vX.Y.Z-(beta|rc(.I))-s`, create from `v1-s`
   - [ ] Android 13 (~t): tag name is `vX.Y.Z-(beta|rc(.I))-t`, create from `v1-t`
5. [ ] Start jobs from [tag pipelines](https://gitlab.e.foundation/e/os/releases/-/pipelines?scope=tags&page=1)
   - [ ] Android 10 (~q): `trigger-build-q-test` and `trigger-build-q-stable`
   - [ ] Android 11 (~r): `trigger-build-r-test` and `trigger-build-r-stable`
   - [ ] Android 12 (~s): `trigger-build-s-test` and `trigger-build-s-stable`
   - [ ] Android 13 (~t): `trigger-build-t-test` and `trigger-build-t-stable`
