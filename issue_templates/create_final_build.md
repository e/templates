1. [ ] Merge the [changelog merge request](https://gitlab.e.foundation/e/os/changelog/-/merge_requests) **if it has been approved by all reviewers**
2. There should not be any modification compare to the last Release Candidate. If any, please run a new Release Candidate.
3. [ ] Create [release tags](https://gitlab.e.foundation/e/os/releases/-/tags/new)
   - [ ] Android 10 (~q): tag name is `vX.Y.Z-q`, create from `v1-q`
   - [ ] Android 11 (~r): tag name is `vX.Y.Z-r`, create from `v1-r`
   - [ ] Android 12 (~s): tag name is `vX.Y.Z-s`, create from `v1-s`
   - [ ] Android 13 (~t): tag name is `vX.Y.Z-t`, create from `v1-t`
4. [ ] Start jobs from [tag pipelines](https://gitlab.e.foundation/e/os/releases/-/pipelines?scope=tags&page=1)
   - [ ] Android 10 (~q): `trigger-build-q-test` and `trigger-build-q-stable`
   - [ ] Android 11 (~r): `trigger-build-r-test` and `trigger-build-r-stable`
   - [ ] Android 12 (~s): `trigger-build-s-test` and `trigger-build-s-stable`
   - [ ] Android 13 (~t): `trigger-build-t-test` and `trigger-build-t-stable`
5. [ ] **d+1** Start jobs from [tag pipelines](https://gitlab.e.foundation/e/os/releases/-/pipelines?scope=tags&page=1)
   - [ ] Android 12 (~s): `trigger-build-s-dev`
6. [ ] **d+2** Start jobs from [tag pipelines](https://gitlab.e.foundation/e/os/releases/-/pipelines?scope=tags&page=1)
   - [ ] Android 11 (~r): `trigger-build-r-dev`
7. [ ] **d+3** Start jobs from [tag pipelines](https://gitlab.e.foundation/e/os/releases/-/pipelines?scope=tags&page=1)
   - [ ] Android 10 (~q): `trigger-build-q-dev`
8. [ ] **d+4** Start jobs from [tag pipelines](https://gitlab.e.foundation/e/os/releases/-/pipelines?scope=tags&page=1)
   - [ ] Android 13 (~t): `trigger-build-t-dev`