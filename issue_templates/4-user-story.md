## Summary

<!-- Write the User Story summarizing the feature. For instance As a user I want to be able to sort my preset timers in order to find them more easily -->

**As a** 
**I want to** 
**in order to** 

## Description

<!-- Write the details you have in mind about this feature -->


## Mock-ups

<!-- Provide a mock-up of how it would look like -->

## Validation

<!-- List test case that will be run to validate that the issue is working as expected -->

/label ~"type::User story"

<!-- Add the labels corresponding to your issue by adding a tilde and typing the name of the label you think apply to your issue in the line above. You need to type a tilde before the name of each label you want to apply to the issue. -->
